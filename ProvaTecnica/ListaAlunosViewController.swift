//
//  ListaAlunosViewController.swift
//  ProvaTecnica
//
//  Created by Eder Andrade on 26/04/22.
//

import UIKit

class ListaAlunosViewController: UIViewController, UITableViewDataSource {
    
    struct Aluno {
        var nome: String
        var matricula: String
    }
    
    @IBOutlet weak var tableView: UITableView!
    private var listaDeAlunos = [Aluno]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.iniciarAlunos()
        self.tableView.dataSource = self
    }
    @IBAction func botaosair(_ sender: Any) {
        
        sairDaTela()
    }
    
    func iniciarAlunos() {
        self.listaDeAlunos = [
            Aluno(nome: "Alisson", matricula: "1"),
            Aluno(nome: "Guilherme Arana", matricula: "2"),
            Aluno(nome: "Alex Telles", matricula: "3"),
            Aluno(nome: "Éder Militão", matricula: "4"),
            Aluno(nome: "Marquinhos", matricula: "5"),
            Aluno(nome: "Thiago Silva", matricula: "6"),
            Aluno(nome: "Casemiro", matricula: "7"),
            Aluno(nome: "Philippe Coutinho", matricula: "8"),
            Aluno(nome: "Raphinha", matricula: "9"),
            Aluno(nome: "Neymar Jr", matricula: "10"),
            Aluno(nome: "Vinicius Junior", matricula: "11")
        ]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Aluno", for: indexPath) as? AlunoTableViewCell {
            cell.alunoLabel.text = self.listaDeAlunos[indexPath.row].nome
            cell.matriculaLabel.text = self.listaDeAlunos[indexPath.row].matricula
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeAlunos.count
    }
    
    func sairDaTela() {
        self.dismiss(animated: true)
    }
}

